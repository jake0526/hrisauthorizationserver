import { Meteor } from 'meteor/meteor';
import { render } from 'react-dom';
import { renderRoutes } from '../imports/startup/client/renderRoutes.js';

Meteor.startup(() => {
  render(renderRoutes(), document.getElementById('render-target'));
});