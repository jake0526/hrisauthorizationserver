import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import ReactDom from 'react-dom';

import { withTracker } from 'meteor/react-meteor-data';

class Login extends Component {
	constructor(props) {
		super(props);

		this.onLogin = this.onLogin.bind(this);

		this.query = new URLSearchParams(location.search);
		this.loginChallenge = this.query.get('login_challenge');
	}

	onLogin(event) {
		event.preventDefault();

		// HTTP.post("/add-numbers", {
		// 	data: {
		// 		'a': 2, 
		// 		'b': 5
		// 	}
		// }, function (err, res) {
		// 	console.log(res.data); // 4
		// });

		HTTP.put("http://192.168.1.15:4444/oauth2/auth/requests/login/" + this.loginChallenge + "/accept", {}, 
			function (err, res) {
				console.log(res); // 4
			}
		);
	}

	render() {
		return (
			<div className="container" style={{marginTop: 50}}>
				<form onSubmit={this.onLogin}>
                    <div className="form-group">
                        <label htmlFor="username">Username</label>
                        <input type="text" ref="username" className="form-control" id="username" placeholder="Username" required/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="password">Password</label>
                        <input type="password" ref="password" className="form-control" id="password" placeholder="Password" required/>
                    </div>
                    <button type="submit" className="btn btn-primary">Login</button>
                </form>
			</div>
		);
	}
};

export default withTracker(() => {
	return {
	};
})(Login);