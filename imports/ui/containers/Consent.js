import React, { Component } from 'react';
import ReactDom from 'react-dom';

import { withTracker } from 'meteor/react-meteor-data';

class Consent extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<div className="container">
				<div className="row">
                    <div className="col-sm-12">
                        <h3> This app wants to access your user data </h3>
                        <h5> User Data </h5>
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm-6">
                        <button type="button" className="btn btn-primary">Accept</button>
                    </div>
                    <div className="col-sm-6">
                        <button type="button" className="btn btn-danger">Reject</button>
                    </div>
                </div>
			</div>
		);
	}
};

export default withTracker(() => {
	return {
	};
})(Consent);