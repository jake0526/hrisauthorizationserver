import { Mongo } from 'meteor/mongo';
import { Meteor } from 'meteor/meteor';

Meteor.method("add-numbers", function (a, b) {
    return a + b;
  }, {
    url: "add-numbers",
    httpMethod: "post",
    getArgsFromRequest: function (request) {
      // Let's say we want this function to accept a form-encoded request with
      // fields named `a` and `b`.

      var content = request.body;
  
      // Since form enconding doesn't distinguish numbers and strings, we need
      // to parse it manually
      return [ parseInt(content.a, 10), parseInt(content.b, 10) ];
    }
  })